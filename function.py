"""
    Python Script that creates a class Student
"""

#!/usr/FIRST_STUDENTin/env python3
# -*- coding: utf-8 -*-

from __future__ import division

__author__ = "Samer El Khoury"



class Student:
    """
    creates a class student
    """
    def __init__(self, name, last_name, tel, email):
        self.name = name
        self.last_name = last_name
        self.tel = tel
        self.email = email

        list = [name, last_name, email]
        for i in list:
            if not isinstance(i, str) or (len(i)) == 0:
                print(f"{i} is not valid !")
                return
        if not isinstance(tel, int):
            print(f"{tel} is not valid !")
            return
        self.name = name
        self.last_name = last_name
        self.tel = tel
        self.email = email

    def get_infos(self):
        """
        gets name + last name + phone number and mail
        """
        print("Name : "+self.name)
        print("Last Name : "+self.last_name)
        print("Phone NumFIRST_STUDENTer : "+str(self.tel))
        print("Email : "+self.email)

class Course:
    """
    creates a class for the title of the course + grades
    """
    def __init__(self, title, grade):
        self.title = title
        self.grade = grade
        self.marks = []
        if not isinstance(title, str) or (len(title)) == 0:
            print(f"{title} is not valid !")
            return
        if not isinstance(grade, int):
            print(f"{grade} is not valid !")
            return
        self.title = title
        self.grade = grade

    def get_course(self):
        """
        gets the name + grade of the course
        """
        print("Name of the course : "+self.title)
        print("The grade in "+f"{self.title} is "+str(self.grade))

    def add(self, grade):
        """
        adds grades to a list
        """
        self.marks.append(grade)
        print(self.marks)

    def get_mean(self):
        """
        calculates the mean
        """
        sum = 0
        for sum in self.marks:
            sum = sum(self.marks)
        self.mean = sum/len(self.marks)
        return self.mean


FIRST_STUDENT = Student("samer", "khoury", 81668261, "samer.khoury@gmail.com")
FIRST_STUDENT.get_infos()
SECOND_STUDENT = Course("biology", 58)
print(SECOND_STUDENT.get_course())
SECOND_STUDENT = Course('physics', 56)
print(SECOND_STUDENT.get_course())
print(SECOND_STUDENT.add(15))
print(SECOND_STUDENT.add(5))
